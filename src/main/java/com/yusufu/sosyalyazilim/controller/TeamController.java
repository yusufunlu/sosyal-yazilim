package com.yusufu.sosyalyazilim.controller;


import com.yusufu.sosyalyazilim.exception.IdCannotBeEmpty;
import com.yusufu.sosyalyazilim.exception.IdMustBeEmpty;
import com.yusufu.sosyalyazilim.exception.NoSuchATeamWithThisId;
import com.yusufu.sosyalyazilim.model.Contract;
import com.yusufu.sosyalyazilim.model.Player;
import com.yusufu.sosyalyazilim.model.Team;
import com.yusufu.sosyalyazilim.model.dto.TeamDto;
import com.yusufu.sosyalyazilim.repositories.ContractRepository;
import com.yusufu.sosyalyazilim.repositories.TeamRepository;
import com.yusufu.sosyalyazilim.service.TeamDataService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping(value = "/team")
public class TeamController {

    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private ContractRepository contractRepository;

    @Autowired
    private TeamDataService teamDataService;


    @PostMapping(value = "/save")
    public TeamDto insertTeam(@RequestBody TeamDto teamDto) {

        if(teamDto.getTeam().getId() != null) {
            throw new IdMustBeEmpty(teamDto.getTeam().getId());
        }
        return teamDataService.saveTeamDetail(teamDto);
    }

    @PutMapping(value = "/update")
    public TeamDto updateTeam(@RequestBody TeamDto teamDto) {

        if(teamDto.getTeam() == null) {
            throw new IdCannotBeEmpty("TeamIdCannotBeEmpty");
        }

        Optional<Team> toUpdateTeam = teamRepository.findById(teamDto.getTeam().getId());
        if(toUpdateTeam.isPresent()) {
            return teamDataService.saveTeamDetail(teamDto);
        } else {
            throw new NoSuchATeamWithThisId(teamDto.getTeam().getId());
        }
    }

    @GetMapping(value = "/list")
    public List<Team> listTeams() {
        return teamRepository.findAll();
    }

    @GetMapping(value = "/detail/{id}")
    public TeamDto getTeamDetail(@PathVariable Long id) {
        if(id == null) {
            throw new IdCannotBeEmpty("TeamIdCannotBeEmpty");
        }

        return teamDataService.getTeamDetail(id);
    }

    @PostMapping(value = "/detail/{id}")
    public List<Player> getPlayerListByTeamAndYear(@RequestBody Integer year, @PathVariable Long id) {
        if(id == null) {
            throw new IdCannotBeEmpty("TeamIdCannotBeEmpty");
        }

        return teamDataService.getPlayerListByTeamAndYear(id,year );
    }

    @DeleteMapping(value = "/delete")
    public void deleteTeam(@RequestBody Long id) {

        if(id == null) {
            throw new IdCannotBeEmpty("TeamIdCannotBeEmpty");
        }
        teamDataService.deleteTeam(id);
    }

}
