package com.yusufu.sosyalyazilim.controller;


import com.yusufu.sosyalyazilim.service.PlayerDataService;
import com.yusufu.sosyalyazilim.exception.IdCannotBeEmpty;
import com.yusufu.sosyalyazilim.exception.NoSuchAPlayerWithThisId;
import com.yusufu.sosyalyazilim.exception.IdMustBeEmpty;
import com.yusufu.sosyalyazilim.model.dto.PlayerDto;
import com.yusufu.sosyalyazilim.model.Player;
import com.yusufu.sosyalyazilim.repositories.ContractRepository;
import com.yusufu.sosyalyazilim.repositories.PlayerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping(value = "/player")
public class PlayerController {

    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private ContractRepository contractRepository;

    @Autowired
    private PlayerDataService playerDataService;


    @PostMapping(value = "/save")
    public PlayerDto insertPlayer(@RequestBody PlayerDto playerDto) {

        if(playerDto.getPlayer().getId() != null) {
            throw new IdMustBeEmpty(playerDto.getPlayer().getId());
        }
        return playerDataService.savePlayerDetail(playerDto);
    }

    //Put idompotent oldugundan update restful api felsefesine göre HTTP put ile yapılmalı ancak bütün restfull ilkelerine uymak zorunda değiliz
    //Mesela hatalar http kodları ile mi(400 gibi) yoksa 200 içinde mesaj ile mi dönmeli => 200 seçtim bu projede
    @PutMapping(value = "/update")
    public PlayerDto updatePlayer(@RequestBody PlayerDto playerDto) {

        if(playerDto.getPlayer() == null) {
            throw new IdCannotBeEmpty("PlayerIdCannotBeEmpty");
        }

        Optional<Player> toUpdatePlayer = playerRepository.findById(playerDto.getPlayer().getId());
        if(toUpdatePlayer.isPresent()) {
            return playerDataService.savePlayerDetail(playerDto);
        } else {
            throw new NoSuchAPlayerWithThisId(playerDto.getPlayer().getId());
        }
    }

    @GetMapping(value = "/list")
    public List<Player> listPlayers() {
        return playerRepository.findAll();
    }

    //Futbolcu geçmişini contratlar üzerinden yaptım ama bu eksik.
    //Daha erken ayrılmış olabilir veya erken ayrılma durumunda contract gerçek tarih ile update edilmesi gerekir
    //Bu da başka sorunlar getirir, bu yüzden en güzeli contract'tan bağımsız ayrı bir team-player mapping entity'si
    //en doğrusu olur ama böyle yapmadım acelem var
    @PostMapping(value = "/detail")
    public PlayerDto getPlayerDetail(@RequestBody Long id) {
        if(id == null) {
            throw new IdCannotBeEmpty("PlayerIdCannotBeEmpty");
        }

        return playerDataService.getPlayerDetail(id);
    }

    @DeleteMapping(value = "/delete")
    public void deletePlayer(@RequestBody Long id) {

        if(id == null) {
            throw new IdCannotBeEmpty("PlayerIdCannotBeEmpty");
        }
        playerDataService.deletePlayer(id);
    }

}
