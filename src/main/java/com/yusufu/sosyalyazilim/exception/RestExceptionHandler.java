package com.yusufu.sosyalyazilim.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.NonUniqueResultException;
import java.lang.reflect.InvocationTargetException;
import java.util.NoSuchElementException;

import static com.yusufu.sosyalyazilim.util.JacksonUtil.parseObjectAsPrettyString;
import static org.springframework.http.HttpStatus.OK;

@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String error = "Malformed JSON request";
        return buildResponseEntity(new ApiError(HttpStatus.BAD_REQUEST, error, ex));
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotWritable(HttpMessageNotWritableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String error = "internal server error";
        return buildResponseEntity(new ApiError(HttpStatus.OK, error, ex));
    }

    private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }

    @ExceptionHandler({
            IncorrectResultSizeDataAccessException.class,
            NonUniqueResultException.class,
            DataIntegrityViolationException.class,
            NoSuchElementException.class,
            IllegalArgumentException.class,
            InvalidDataAccessApiUsageException.class,
            RuntimeException.class,
            Exception.class,
            InvocationTargetException.class
    })
    @Nullable
    protected ResponseEntity<Object> handleException(Exception ex) {
        ApiError apiError = new ApiError(OK);
        apiError.setMessage(ex.getMessage());
        log.error(parseObjectAsPrettyString(apiError));
        return buildResponseEntity(apiError);
    }


}
