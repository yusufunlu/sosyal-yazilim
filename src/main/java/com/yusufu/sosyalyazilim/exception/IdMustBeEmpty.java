package com.yusufu.sosyalyazilim.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class IdMustBeEmpty extends RuntimeException {

    public IdMustBeEmpty(Long id) {
        super(String.format("%s%d", "Player Id must be empty ", id));
    }
}