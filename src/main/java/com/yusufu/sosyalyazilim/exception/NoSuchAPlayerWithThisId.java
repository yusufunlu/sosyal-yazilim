package com.yusufu.sosyalyazilim.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class NoSuchAPlayerWithThisId extends RuntimeException {

    public NoSuchAPlayerWithThisId(Long id) {
        super(String.format("%s%d", "There is no player with this id: ", id));
    }
}