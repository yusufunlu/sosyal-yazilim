package com.yusufu.sosyalyazilim.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class NoSuchAContractWithThisId extends RuntimeException {

    public NoSuchAContractWithThisId(Long id) {
        super(String.format("%s%d", "There is no team with this id: ", id));
    }
}