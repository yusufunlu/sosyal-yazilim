package com.yusufu.sosyalyazilim.repositories;

import com.yusufu.sosyalyazilim.model.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface PlayerRepository extends JpaRepository<Player,Long> {
    List<Player> findByNameIgnoreCaseContaining(String name);
    Player findByName(String name);
    List<Player> findByNameIn(List<String> nameList);
    Optional<Player> findOneByName(String name);
    List<Player> findByIdIn(List<Long> IdList);
    List<Player> findByIdNotIn(List<Long> IdList);

    //@Query(value = "SELECT p FROM Player p INNER JOIN Contract c on c.teamId = :teamId WHERE CURRENT_TIME  BETWEEN c.start AND c.end")
    @Query(value = "SELECT p,c FROM Player p INNER JOIN Contract c on c.teamId = :teamId ")
    List<Object[]> takimGetir(@Param("teamId") Long teamId);
}
