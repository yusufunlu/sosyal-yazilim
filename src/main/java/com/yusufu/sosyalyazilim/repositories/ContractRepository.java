package com.yusufu.sosyalyazilim.repositories;

import com.yusufu.sosyalyazilim.model.Contract;
import com.yusufu.sosyalyazilim.model.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface ContractRepository extends JpaRepository<Contract,Long> {
    List<Contract> findByTeamIdIn(List<Long> teamIdList);
    List<Contract> findByTeamId(Long playerId);

    List<Contract> findByPlayerIdIn(List<Long> playerIdList);
    List<Contract> findByPlayerId(Long playerId);

    void deleteByPlayerId(Long playerId);

    void deleteByTeamId(Long id);
}
