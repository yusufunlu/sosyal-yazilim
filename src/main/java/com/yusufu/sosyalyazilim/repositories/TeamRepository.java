package com.yusufu.sosyalyazilim.repositories;

import com.yusufu.sosyalyazilim.model.Player;
import com.yusufu.sosyalyazilim.model.Team;
import com.yusufu.sosyalyazilim.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface TeamRepository extends JpaRepository<Team,Long> {
    List<Team> findByNameIgnoreCaseContaining(String name);
    Team findByName(String name);
    List<Team> findByNameIn(List<String> nameList);
    Optional<Team> findOneByName(String name);
    List<Team> findByIdIn(List<Long> IdList);
    List<Team> findByIdNotIn(List<Long> IdList);

}
