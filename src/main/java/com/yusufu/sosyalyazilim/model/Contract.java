package com.yusufu.sosyalyazilim.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Contract {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contract_generator")
    @SequenceGenerator(name="contract_generator", sequenceName = "contract_seq" , allocationSize = 1)
    protected Long id;
    @Column(name="teamId")
    private Long teamId;
    private Long playerId;
    private String teamName;
    private String playerName;
    @Temporal(TemporalType.TIMESTAMP)
    private Date start;
    @Temporal(TemporalType.TIMESTAMP)
    private Date end;
}
