package com.yusufu.sosyalyazilim.model.dto;

import com.yusufu.sosyalyazilim.model.Contract;
import com.yusufu.sosyalyazilim.model.Player;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class PlayerDto {

    private Player player;
    private List<Contract> contracts = new ArrayList<>();

    public PlayerDto(Player player) {
        this.player = player;
    }
}
