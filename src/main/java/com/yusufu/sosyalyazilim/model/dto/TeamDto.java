package com.yusufu.sosyalyazilim.model.dto;

import com.yusufu.sosyalyazilim.model.Contract;
import com.yusufu.sosyalyazilim.model.Player;
import com.yusufu.sosyalyazilim.model.Team;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class TeamDto {

    private Team team;
    private List<Contract> contracts = new ArrayList<>();

    public TeamDto(Team team) {
        this.team = team;
    }
}
