package com.yusufu.sosyalyazilim.model.mapping;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.io.Serializable;

@Getter
@Setter
@Entity
@NoArgsConstructor
@IdClass(TeamPlayerMapping.class)
public class TeamPlayerMapping implements Serializable {

    @Id
    private Long playerId;
    @Id
    private Long teamId;

    public TeamPlayerMapping(Long playerId, Long teamId) {
        this.playerId = playerId;
        this.teamId = teamId ;
    }
}
