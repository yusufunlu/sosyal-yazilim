package com.yusufu.sosyalyazilim.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "team_generator")
    @SequenceGenerator(name="team_generator", sequenceName = "team_seq" , allocationSize = 1)
    protected Long id;
    private String name;

    public Team(String name) {
        this.name = name;
    }
}
