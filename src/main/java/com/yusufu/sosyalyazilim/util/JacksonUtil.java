package com.yusufu.sosyalyazilim.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JacksonUtil {

    public static String parseObjectAsPlaintString(Object object) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            log.error("CUSTOM JACKSON LOG HAS THIS EXCEPTION:  "+ e.getMessage());
            return "";
        }
    }

    public static String parseObjectAsPrettyString(Object object) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            log.error("CUSTOM JACKSON LOG HAS THIS EXCEPTION:  "+ e.getMessage());
            return "";
        }
    }

}
