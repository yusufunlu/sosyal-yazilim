package com.yusufu.sosyalyazilim.service;

import com.yusufu.sosyalyazilim.exception.NoSuchAPlayerWithThisId;
import com.yusufu.sosyalyazilim.model.Contract;
import com.yusufu.sosyalyazilim.model.dto.PlayerDto;
import com.yusufu.sosyalyazilim.model.Player;
import com.yusufu.sosyalyazilim.repositories.ContractRepository;
import com.yusufu.sosyalyazilim.repositories.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PlayerDataService {

    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private ContractRepository contractRepository;

    @Transactional
    public PlayerDto savePlayerDetail(PlayerDto playerDto) {

        Player savedPlayer = playerRepository.save(playerDto.getPlayer());

        playerDto.getContracts().stream().forEach(contract -> {
            contract.setPlayerId(playerDto.getPlayer().getId());
            contract.setPlayerName(playerDto.getPlayer().getName());
        });
        List<Contract> savedContractList = contractRepository.saveAll(playerDto.getContracts());

        return new PlayerDto(savedPlayer, savedContractList);

    }

    public PlayerDto getPlayerDetail(Long id) {

        PlayerDto playerDetail = null;

        Optional<Player> playerOptional = playerRepository.findById(id);
        if(playerOptional.isPresent()) {
            List<Contract> contractList = contractRepository.findByPlayerId(id);
            playerDetail = new PlayerDto(playerOptional.get(), contractList);
        } else {
            throw new NoSuchAPlayerWithThisId(id);
        }
        return playerDetail;

    }

    @Transactional
    public void deletePlayer(Long id) {

        Optional<Player> toUpdatePlayer = playerRepository.findById(id);
        if(toUpdatePlayer.isPresent()) {
            playerRepository.deleteById(id);
            contractRepository.deleteByPlayerId(id);
        } else {
            throw new NoSuchAPlayerWithThisId(id);
        }

    }
}
