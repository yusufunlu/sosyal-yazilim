package com.yusufu.sosyalyazilim.service;

import com.yusufu.sosyalyazilim.exception.NoSuchATeamWithThisId;
import com.yusufu.sosyalyazilim.model.Contract;
import com.yusufu.sosyalyazilim.model.Player;
import com.yusufu.sosyalyazilim.model.Team;
import com.yusufu.sosyalyazilim.model.dto.TeamDto;
import com.yusufu.sosyalyazilim.repositories.ContractRepository;
import com.yusufu.sosyalyazilim.repositories.PlayerRepository;
import com.yusufu.sosyalyazilim.repositories.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class TeamDataService {

    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private ContractRepository contractRepository;
    @Autowired
    private PlayerRepository playerRepository;
    @Transactional
    public TeamDto saveTeamDetail(TeamDto teamDto) {

        Team savedTeam = teamRepository.save(teamDto.getTeam());

        teamDto.getContracts().stream().forEach(contract -> {
            contract.setTeamId(teamDto.getTeam().getId());
            contract.setTeamName(teamDto.getTeam().getName());
        });
        List<Contract> savedContractList = contractRepository.saveAll(teamDto.getContracts());

        return new TeamDto(savedTeam, savedContractList);

    }

    public TeamDto getTeamDetail(Long id) {

        TeamDto teamDetail = null;

        Optional<Team> teamOptional = teamRepository.findById(id);
        if(teamOptional.isPresent()) {
            List<Contract> contractList = contractRepository.findByTeamId(id);
            teamDetail = new TeamDto(teamOptional.get(), contractList);
        } else {
            throw new NoSuchATeamWithThisId(id);
        }
        return teamDetail;

    }
    public List<Player> getPlayerListByTeamAndYear(Long id, Integer year) {

        Date date = new Date();
        date.setYear(year);
        //List<Player> playerList = playerRepository.takimGetir(id);
        List<Long> playerIdList =  contractRepository.findByTeamId(id).stream().filter(contract -> contract.getStart().getYear() <= year
                && contract.getEnd().getYear() >= year).map(contract -> contract.getPlayerId()).collect(Collectors.toList());

        List<Player> playerList = playerRepository.findByIdIn(playerIdList);
        return playerList;
    }

    @Transactional
    public void deleteTeam(Long id) {

        Optional<Team> toUpdateTeam = teamRepository.findById(id);
        if(toUpdateTeam.isPresent()) {
            teamRepository.deleteById(id);
            contractRepository.deleteByTeamId(id);
        } else {
            throw new NoSuchATeamWithThisId(id);
        }

    }
}
