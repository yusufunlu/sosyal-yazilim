package com.yusufu.sosyalyazilim;

import com.yusufu.sosyalyazilim.controller.PlayerController;
import com.yusufu.sosyalyazilim.controller.TeamController;
import com.yusufu.sosyalyazilim.model.Contract;
import com.yusufu.sosyalyazilim.model.Player;
import com.yusufu.sosyalyazilim.model.Role;
import com.yusufu.sosyalyazilim.model.Team;
import com.yusufu.sosyalyazilim.model.dto.PlayerDto;
import com.yusufu.sosyalyazilim.model.dto.TeamDto;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.Date;
import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc //need this in Spring Boot test
@ActiveProfiles("h2")
@SpringJUnitConfig
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class TestCases {

    @Autowired
    private TeamController teamController;
    @Autowired
    private PlayerController playerController;

    @Test
    public void scenario2(){

        insertTestData();
        List<Player> playerList = playerController.listPlayers();
        PlayerDto playerDto = playerController.getPlayerDetail(playerList.get(0).getId());

        Assert.assertNotNull(playerDto.getContracts());
        Assert.assertTrue(playerDto.getContracts().size() > 0);
    }

    @Test
    public void scenario3(){

        insertTestData();
        List<Player> teamList = teamController.getPlayerListByTeamAndYear(2019, 1L);
        Assert.assertTrue(teamList.size() > 0);
    }

    private void insertTestData() {
        Team team = new Team("Barcelone FC");
        TeamDto teamDto = new TeamDto(team);
        TeamDto savedTeamDto = teamController.insertTeam(teamDto);

        Team team2 = new Team("Real Madrid FC");
        TeamDto teamDto2 = new TeamDto(team2);
        TeamDto savedTeamDto2 = teamController.insertTeam(teamDto2);

        Player messy = new Player(null, "Messy", "Cash", 33, Role.Keeper);
        PlayerDto messyDto = new PlayerDto(messy);
        playerController.insertPlayer(messyDto);

        Player ronaldo = new Player(null, "Ronaldo", "Cash", 27, Role.Keeper);
        PlayerDto ronaldoDto = new PlayerDto(ronaldo);
        playerController.insertPlayer(ronaldoDto);


        Date firstYear = new Date();
        Date secondYear = new Date();
        Date thirdYear = new Date();
        firstYear.setYear(2018);
        secondYear.setYear(2019);

        Contract messyBarceloneContract = new Contract(null, savedTeamDto.getTeam().getId(),messyDto.getPlayer().getId(),
                teamDto.getTeam().getName(), messyDto.getPlayer().getName(), firstYear, secondYear);

        Contract messyRealContract = new Contract(null, savedTeamDto2.getTeam().getId(),messyDto.getPlayer().getId(),
                teamDto.getTeam().getName(), messyDto.getPlayer().getName(), secondYear, thirdYear);

        Contract ronaldoBarceloneContract = new Contract(null, savedTeamDto.getTeam().getId(),ronaldoDto.getPlayer().getId(),
                teamDto.getTeam().getName(), messyDto.getPlayer().getName(), firstYear, secondYear);

        Contract ronaldoRealContract = new Contract(null, savedTeamDto2.getTeam().getId(),ronaldoDto.getPlayer().getId(),
                teamDto.getTeam().getName(), messyDto.getPlayer().getName(), secondYear, thirdYear);

        messyDto.getContracts().add(messyBarceloneContract);
        messyDto.getContracts().add(messyRealContract);

        ronaldoDto.getContracts().add(ronaldoBarceloneContract);
        ronaldoDto.getContracts().add(ronaldoRealContract);

        PlayerDto updatedMessy = playerController.updatePlayer(messyDto);
        PlayerDto updatedRonaldo = playerController.updatePlayer(messyDto);
    }
}
