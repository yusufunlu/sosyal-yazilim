package com.yusufu.sosyalyazilim;

import com.yusufu.sosyalyazilim.controller.PlayerController;
import com.yusufu.sosyalyazilim.controller.TeamController;
import com.yusufu.sosyalyazilim.model.Contract;
import com.yusufu.sosyalyazilim.model.Player;
import com.yusufu.sosyalyazilim.model.Role;
import com.yusufu.sosyalyazilim.model.Team;
import com.yusufu.sosyalyazilim.model.dto.PlayerDto;
import com.yusufu.sosyalyazilim.model.dto.TeamDto;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.Date;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc //need this in Spring Boot test
@ActiveProfiles("h2")
@SpringJUnitConfig
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class TeamPlayerMappingTest {

    @Autowired
    private TeamController teamController;
    @Autowired
    private PlayerController playerController;

    @Test
    public void saveTeamTest(){

        Team team = new Team("Software FC");
        TeamDto teamDto = new TeamDto(team);
        TeamDto savedTeamDto = teamController.insertTeam(teamDto);

        Player player = new Player(null, "Jonny", "Cash", 33, Role.Keeper);
        PlayerDto playerDto = new PlayerDto(player);
        PlayerDto savePlayerDetail = playerController.insertPlayer(playerDto);

        Date startDate = new Date();
        Date finishDate = new Date();
        finishDate.setYear(startDate.getYear()+1);
        Contract contract = new Contract(null, savedTeamDto.getTeam().getId(),playerDto.getPlayer().getId(),
                teamDto.getTeam().getName(), playerDto.getPlayer().getName(), startDate, finishDate);

        playerDto.getContracts().add(contract);
        PlayerDto updatedPlayer = playerController.updatePlayer(playerDto);


        Assert.assertEquals(savedTeamDto.getTeam().getName(),team.getName());
        Assert.assertEquals(savePlayerDetail.getPlayer().getName(),player.getName());
        Assert.assertEquals(updatedPlayer.getContracts().get(0).getPlayerName(),player.getName());
        Assert.assertEquals(updatedPlayer.getContracts().get(0).getTeamName(),team.getName());

    }
}
