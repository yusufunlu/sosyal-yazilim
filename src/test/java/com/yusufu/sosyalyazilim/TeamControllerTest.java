package com.yusufu.sosyalyazilim;

import com.yusufu.sosyalyazilim.controller.TeamController;
import com.yusufu.sosyalyazilim.model.Contract;
import com.yusufu.sosyalyazilim.model.Team;
import com.yusufu.sosyalyazilim.model.dto.TeamDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc //need this in Spring Boot test
@ActiveProfiles("h2")
@SpringJUnitConfig
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class TeamControllerTest {


    @Autowired
    private TeamController teamController;


    @Test
    public void saveTeamTest(){

        Team team = new Team("Software FC");
        TeamDto teamDto = new TeamDto(team);
        TeamDto savedTeamDto = teamController.insertTeam(teamDto);
        Assert.assertEquals(savedTeamDto.getTeam(),team.getName());
    }

}
